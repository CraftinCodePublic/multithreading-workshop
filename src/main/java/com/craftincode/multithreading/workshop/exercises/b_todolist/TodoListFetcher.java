package com.craftincode.multithreading.workshop.exercises.b_todolist;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.ArrayList;
import java.util.List;

/*
    App fetching "to do" entries from https://jsonplaceholder.typicode.com/
 */
public class TodoListFetcher {
    private static final int MAX_ID = 190;
    private static final boolean useMultithreading = false;

    public static void main(String[] args) {
        long startTime = System.nanoTime();

        List<String> todos;
        if (useMultithreading) {
            System.out.println("--- MULTITHREADED VERSION ---");
            todos = fetchTodosMultithreaded();
        } else {
            System.out.println("--- SINGLE THREAD VERSION ---");
            todos = fetchTodosOneThread();
        }

        long duration = System.nanoTime() - startTime;
        double durationInSeconds = duration / 1000000000d;

        System.out.println("Data fetching ended, fetched entries number: " + todos.size());
        System.out.println("Duration: " + durationInSeconds + "s");
    }

    private static List<String> fetchTodosMultithreaded() {
        List<Thread> threads = new ArrayList<>();
        final List<String> todos = new ArrayList<>();

        for (int i = 1; i <= MAX_ID; i++) {
            int finalI = i;
            Thread thread = new Thread(){
                @Override
                public void run() {
                    todos.add(getTodo(finalI));
                }
            };
            thread.start();
            threads.add(thread);
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // TODO create multithreaded todos fetching mechanism
        return todos;
    }

    private static List<String> fetchTodosOneThread() {
        final List<String> todos = new ArrayList<>();
        for (int i = 1; i <= MAX_ID; i++) {
            todos.add(getTodo(i));
        }
        return todos;
    }

    /*
        Method fetching "to do" entry from https://jsonplaceholder.typicode.com/todos/{id}
     */
    private static String getTodo(int id) {
        try {
            return Unirest.get("https://jsonplaceholder.typicode.com/todos/" + id).asJson().getBody().getObject().getString("title");
        } catch (UnirestException e) {
            e.printStackTrace();
            return null;
        }
    }
}
