package com.craftincode.multithreading.workshop.exercises.c_primes;

import com.craftincode.multithreading.workshop.exercises.Algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class PrimeNumbers {
    public static final long MAX_NUMBER = 6000000;
    private static final boolean USE_MULTITHREADING = true;

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        long startTime = System.nanoTime();

        List<Long> primesFromRange;
        if (USE_MULTITHREADING) {
            System.out.println("--- MULTITHREADED VERSION ---");
            primesFromRange = findPrimesUsingMultithreading(1, MAX_NUMBER);
        } else {
            System.out.println("--- SINGLE THREAD VERSION ---");
            primesFromRange = Algorithms.findPrimesFromRange(1, MAX_NUMBER);
        }

        System.out.println("Found " + primesFromRange.size() + " prime numbers");

        long duration = System.nanoTime() - startTime;
        double durationInSeconds = duration / 1000000000d;
        System.out.println("Duration: " + durationInSeconds + "s");
    }

    private static List<Long> findPrimesUsingMultithreading(long start, long end) throws ExecutionException, InterruptedException {

        Callable<List<Long>> callable1 = new Callable<List<Long>>() {
            @Override
            public List<Long> call() throws Exception {
                return Algorithms.findPrimesFromRange(start, end/2);
            }
        };

        Callable<List<Long>> callable2 = new Callable<List<Long>>() {
            @Override
            public List<Long> call() throws Exception {
                return Algorithms.findPrimesFromRange(end/2, end);
            }
        };

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        Future<List<Long>> future1 = executorService.submit(callable1);
        Future<List<Long>> future2 = executorService.submit(callable2);

        List<Long> result1 = future1.get();
        List<Long> result2 = future2.get();

        result1.addAll(result2);

        return result1;
    }
}
