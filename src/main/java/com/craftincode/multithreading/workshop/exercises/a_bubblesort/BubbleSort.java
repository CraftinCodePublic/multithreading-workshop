package com.craftincode.multithreading.workshop.exercises.a_bubblesort;

import com.craftincode.multithreading.workshop.exercises.Algorithms;

public class BubbleSort {
    private static final int SIZE = 60000;

    public static void main(String[] args) throws InterruptedException {
        // Generating random array with specified SIZE
        int[] array1 = Algorithms.createRandomArray(SIZE);
        int[] array2 = Algorithms.createRandomArray(SIZE);

        // Duration counting start
        long startTime = System.nanoTime();

        System.out.println("[PRE] Is array1 sorted: " + Algorithms.isArraySorted(array1));
        System.out.println("[PRE] Is array2 sorted: " + Algorithms.isArraySorted(array2));

        // TODO create mechanism for running bubble sort in parallel
        // using 2 threads

        Thread thread1 = new Thread(){
            @Override
            public void run() {
                Algorithms.bubbleSort(array1);
            }
        };
        Thread thread2 = new Thread(){
            @Override
            public void run() {
                Algorithms.bubbleSort(array2);
            }
        };

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();


        System.out.println("[POST] Is array1 sorted: " + Algorithms.isArraySorted(array1));
        System.out.println("[POST] Is array2 sorted: " + Algorithms.isArraySorted(array2));

        // Duration counting end, summary
        long duration = System.nanoTime() - startTime;
        double durationInSeconds = duration / 1000000000d;
        System.out.println("Duration " + durationInSeconds + "s");

    }
}
