package com.craftincode.multithreading.workshop.exercises.x_exercise1;

public class MyThread1 extends Thread {
    @Override
    public void run() {
        for (int i = 100; i < 200; i++) {
            System.out.println(i);

            try {
                Thread.sleep(100); //100ms = 0.1s
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
