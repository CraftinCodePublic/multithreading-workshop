package com.craftincode.multithreading.workshop.exercises.x_exercise1;

public class ThreadRunner {
    public static void main(String[] args) {
        MyThread1 myThread1 = new MyThread1();
        MyThread2 myThread2 = new MyThread2();

        Thread thread3 = new Thread(){
            @Override
            public void run() {
                for (int i = 300; i < 400; i++) {
                    System.out.println(i);

                    try {
                        Thread.sleep(100); //100ms = 0.1s
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread thread4 = new Thread(){
            @Override
            public void run() {
                for (int i = 400; i < 500; i++) {
                    System.out.println(i);

                    try {
                        Thread.sleep(100); //100ms = 0.1s
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };


        myThread1.start(); // wypisuje 100-200
        myThread2.start(); // wypisuje 200-300
        thread3.start(); // wypisuje 300-400
        thread4.start(); // wypisuje 400-500
    }
}
