package com.craftincode.multithreading.workshop.examples.d_future;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class FuturesCallablesPool {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        List<Future<String>> futuresList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Callable<String> callable = createCallable("Callable_" + i);
            Future<String> future = executorService.submit(callable);
            futuresList.add(future);
        }
        System.out.println("Callables executed, getting future outputs");

        for (Future<String> future : futuresList) {
            String futureOutput = future.get();
            System.out.println("Future output: " + futureOutput);
        }
    }

    private static Callable<String> createCallable(String name) throws InterruptedException {
        return new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(2000);
                return "This is answer from " + name;
            }
        };
    }
}
