package com.craftincode.multithreading.workshop.examples.b_runnable;

public class CreatingAnonymousRunnable {
    public static void main(String[] args) {
        System.out.println("Starting main method");

        Runnable runnable = new Runnable() {
            public void run() {
                System.out.println("Yay, I am a runnable!");
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();

        System.out.println("Ending main method");
    }
}
