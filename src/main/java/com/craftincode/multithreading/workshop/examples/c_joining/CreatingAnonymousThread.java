package com.craftincode.multithreading.workshop.examples.c_joining;

public class CreatingAnonymousThread {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Starting main method");

        Thread thread1 = new Thread(){
            @Override
            public void run() {
                System.out.println("Yay, I am a thread #1");
            }
        };
        Thread thread2 = new Thread(){
            @Override
            public void run() {
                System.out.println("Yay, I am a thread #2");
            }
        };

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();


        System.out.println("Ending main method");
    }
}
