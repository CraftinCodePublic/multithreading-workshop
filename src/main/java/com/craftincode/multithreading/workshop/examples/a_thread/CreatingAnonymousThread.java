package com.craftincode.multithreading.workshop.examples.a_thread;

public class CreatingAnonymousThread {
    public static void main(String[] args) {
        System.out.println("Starting main method");
        Thread thread = new Thread(){
            @Override
            public void run() {
                System.out.println("Yay, I am a thread!");
            }
        };
        thread.start();
        System.out.println("Ending main method");
    }
}
