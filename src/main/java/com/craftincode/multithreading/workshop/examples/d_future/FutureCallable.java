package com.craftincode.multithreading.workshop.examples.d_future;

import java.util.concurrent.*;

public class FutureCallable {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        Callable<String> callable = createCallable("callable_1");

        System.out.println("Executing callable...");
        Future<String> future = executorService.submit(callable);
        System.out.println("Callable executed, getting future output");

        String futureResult = future.get();
        System.out.println("Result from future: " + futureResult);
    }

    private static Callable<String> createCallable(String name) {
        return new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(2000);
                return "This is answer from " + name;
            }
        };
    }
}
